window.B4 =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  link_search: (tag) ->
    window.B4.links_router.navigate("tagged/" + tag, {trigger: true})

  initialize: ->
    @links_router = new B4.Routers.LinksRouter
    @links_view = new B4.Views.LinksIndex({model: links})
    @new_link_view = new B4.Views.NewLinkForm()
    @links_view.el = $ "#links"
    @new_link_view.el = $ "#new_link_form"
    @new_link_view.render()
    @links_view.render()
    $("#navbar-search-input").typeahead({
      source: (query, process) ->
        $.get('/tags/typeahead', { query: query }, (response) -> process(response.tags))
    })

    Backbone.history.start()

$(document).ready ->
  B4.initialize()
  $("#navbar-search-form").submit((e) ->
    B4.link_search($("#navbar-search-input").val())
    e.preventDefault()
  )
