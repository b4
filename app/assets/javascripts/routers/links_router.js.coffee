class B4.Routers.LinksRouter extends Backbone.Router
  routes: {
    "": "links"
    "tagged/:tag": "tagged"
  }

  links: ->
    window.links.fetch { success: ->
      B4.links_view.current_tag = ''
      B4.links_view.model = window.links
      B4.links_view.render()
    }

  tagged: (tag) ->
    window.links.fetch {data: {tag: decodeURIComponent(tag)}, success: ->
      B4.links_view.current_tag = tag
      B4.links_view.model = window.links
      B4.links_view.render()
    }

