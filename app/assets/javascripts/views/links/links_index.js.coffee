class B4.Views.NewLinkForm extends Backbone.View
  el: $ '#new_link_form'
  tagName: 'div'
  events:
    'click form button' : "onSubmit"

  onSubmit: ->
    $(@el).find('input[type="submit"]').attr("data-loading-text", "Saving...")
    newlink = new B4.Models.Link()
    newlink.set({
      title: $(@el).find('input[name="title"]').val()
      url: $(@el).find('input[name="url"]').val()
      tags: $(@el).find('input[name="tags"]').val()
    })
    newlink.save({}, {success: ->
      $('input[name="title"]').val("")
      $('input[name="url"]').val("")
      $('input[name="tags"]').val("")
      cur = B4.links_view.current_tag
      if cur is "" or cur in newlink.attributes.tags
        window.links.add(newlink, {at: 0})
        B4.links_view.model = window.links
        B4.links_view.render()
      $(@el).find('input[type="submit"]').removeAttr("data-loading-text")
    })

  render: ->
    console.log(@el)
    @el.unbind("submit")
    @el.submit((e) ->
      B4.new_link_view.onSubmit()
      e.preventDefault()
    )
    @

class B4.Views.LinksIndex extends Backbone.View
  el: $ '#links'
  tagName: 'ul'

  render: ->
    $(@el).html @template(links: @model.toJSON())
    @

  template: JST['links/index']

$(document).ready ->
  B4.Views.LinksIndex.prototype.el = $ "#links"
  B4.Views.NewLinkForm.prototype.el = $ "#new_link"
