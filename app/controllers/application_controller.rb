class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!

  def app
    @link = Link.new
    @links = User.find(current_user).links

    render 'app', :layout => 'app' 
  end
end
