class LinksController < ApplicationController
  before_filter :authenticate_user!

  def show
    link = User.find(current_user).links.find(params[:id])
    render :json => link
  end

  def index
    unless params[:tag].blank?
      links = User.find(current_user).links.tagged_with(params[:tag])
    else
      links = User.find(current_user).links
    end
    render :json => links
  end

  def destroy
    links = User.find(current_user).links.find(params[:id]).delete
    render :json => links
  end

  def create
    link = User.find(current_user).links.create!(url: params[:link][:url], title: params[:link][:title])
    link.tag_list = params[:tags]
    link.save
    link.reload
    render :json => link
  end
end
