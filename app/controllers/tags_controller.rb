class TagsController < ApplicationController
  before_filter :authenticate_user!
  def typeahead
    tags = []
    User.find(current_user).links.each do |link|
      tags |= link.tag_list # TODO: this is pretty inefficient. Use one query instead
    end

    unless params[:query].nil?
      tags = tags.select do |that| that.include? params[:query] end
    end

    render :json => {tags: tags}
  end
end
