require 'uri'

class Link < ActiveRecord::Base
  attr_accessible :title, :url, :user_id
  belongs_to :user
  acts_as_taggable

  validates :user_id, presence: true
  validates :url, presence: true

  validates :url, :format => URI::regexp(%w(http https))
  validates :title, :format => /\S/

  default_scope order: 'links.created_at DESC'

  def serializable_hash(options=nil)
    options ||= {}
    super(options).merge({"tags" => self.tag_list})
  end
end
